//
//  RegistercodeViewController.swift
//  Versicherungen App
//
//  Created by brst on 29/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class RegistercodeViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var blankbtn: UIButton!
    @IBOutlet var selectimage: UIImageView!
    @IBOutlet var codetext: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.codetext.delegate = self
        self.selectimage.hidden = true
        self.blankbtn.hidden = false
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.codetext .resignFirstResponder()
    }


    @IBAction func back(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func continuebtn(sender: AnyObject)
    {
        if (self.codetext.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie den Code ein"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
            
        else if (self.codetext.text!.characters.count <= 4)
        {
            
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie den richtigen Code "
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
            
        else
        {
            
            
            let Insurance = self.storyboard!.instantiateViewControllerWithIdentifier("InsuranceregisterViewController") as! InsuranceregisterViewController
            
            self.navigationController?.pushViewController(Insurance, animated: true)
        }
  
    }
    
    @IBAction func refreshbtn(sender: AnyObject)
    {
        
    }
    
    
    
    
}
