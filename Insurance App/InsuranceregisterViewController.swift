//
//  InsuranceregisterViewController.swift
//  Insurance App
//
//  Created by brst on 22/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class InsuranceregisterViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet var insuranceregistertable: UITableView!
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        myMenuView.hidden = true
        self.insuranceregistertable.delegate = self
        currentVC = self
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        myMenuView.hidden = true
        closeMenuBtn.hidden = true
    }

    @IBAction func sidebar(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
//            x = -myMenuView.frame.size.width
//            navx = 0
            closeMenuBtn.hidden = false
        }
        
        
        UIView.animateWithDuration(0.5, animations:
            {
            
            
            myMenuView.frame.origin.x = self.x
            
        })

    }
    
    
    @IBAction func addbtn(sender: AnyObject)
    {
        let SelectInsurance = self.storyboard!.instantiateViewControllerWithIdentifier("SelectInsuranceViewController") as! SelectInsuranceViewController
        
        self.navigationController?.pushViewController(SelectInsurance, animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfSectionsInTable sections:Int) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! InsuranceregisterTableCell
        
        
        
        
        return cell
        
    }

    
}
