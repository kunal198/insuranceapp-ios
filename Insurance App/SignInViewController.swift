//
//  SignInViewController.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    var dataModel = NSData()
//    var reg = String()
    var data = NSData()
    
    @IBOutlet var register: UILabel!
    
    @IBOutlet var nametextfield: UITextField!

    @IBOutlet var mobilenumbertextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
     
//        if reg != ""
//        {
//            register.text = reg
//        }
        
        // Do any additional setup after loading the view.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func viewWillAppear(animated: Bool)
    {
        //self.nametextfield .resignFirstResponder()
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    
    @IBAction func continuebtn(sender: AnyObject)
    {
        if (self.nametextfield.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie den Namen"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
        else if (self.mobilenumbertextfield.text! == "")
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie Handynummer"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
            
            
        else if (self.mobilenumbertextfield.text!.characters.count <= 9)
        {
            let alert = UIAlertView()
            alert.title = "Aufmerksam"
            alert.message = "Bitte geben Sie die 10 Ziffern Handynummer"
            alert.addButtonWithTitle("Ok")
            alert.show()
            
        }
            
            
        else
        {
            
            let post = NSString(format:"name=%@&mobileno=%@",nametextfield.text!,mobilenumbertextfield.text!)
            
            
            //println(post)
            
            dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            let postLength = String(dataModel.length)
            
            let url = NSURL(string: "http://beta.brstdev.com/insurance/backend/index.php/login/register")
            
            let urlRequest = NSMutableURLRequest(URL: url!)
            
            urlRequest.HTTPMethod = "POST"
            urlRequest.HTTPBody = dataModel
            urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
                
   
                do
                {
                    let dicObj = try NSJSONSerialization.JSONObjectWithData(self.data, options: NSJSONReadingOptions()) as? NSDictionary
                    print(dicObj)
                    
                }
                    
                catch let error as NSError
                {
                    print(error.localizedDescription)
                }

                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert = UIAlertView()
                        alert = UIAlertView(title:"Alert", message: "Data not found", delegate: self, cancelButtonTitle:"OK")
                        alert.show()
                        
                        
                    })
                    
                }
                    
                else
                {
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        
                        
                        
                        
                    })
                    
                    
                }
                
                
            })
            task.resume()
        
        
        let EnterCode = self.storyboard!.instantiateViewControllerWithIdentifier("EnterCodeViewController") as! EnterCodeViewController
        
        self.navigationController?.pushViewController(EnterCode, animated: true)
        
        }
    }
 

    //MARK: - Single Item Detail Api
                            
                             
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.mobilenumbertextfield .resignFirstResponder()
    }
    
    
    @IBAction func termsbtn(sender: AnyObject)
    {
        let Terms = self.storyboard!.instantiateViewControllerWithIdentifier("TermsViewController") as! TermsViewController
        
        self.navigationController?.pushViewController(Terms, animated: true)
    }
    
   
    @IBAction func privacypolicybtn(sender: AnyObject)
    {
        let PrivacyPolicy = self.storyboard!.instantiateViewControllerWithIdentifier("PrivacyPolicyViewController") as! PrivacyPolicyViewController
        
        self.navigationController?.pushViewController(PrivacyPolicy, animated: true)
        
    }
    
    
    @IBAction func returntextfield(sender: AnyObject)
    {
        self.nametextfield.resignFirstResponder()
        self.mobilenumbertextfield.resignFirstResponder()
    }
    
    

    }

