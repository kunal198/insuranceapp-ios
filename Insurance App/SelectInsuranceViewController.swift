//
//  SelectInsuranceViewController.swift
//  Insurance App
//
//  Created by brst on 17/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class SelectInsuranceViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    let but = UIButton()
    var indexpath = NSIndexPath()
    var checkbtn = Bool()
    @IBOutlet var blurview: UIView!
    @IBOutlet var newview: UIView!
    @IBOutlet var selectinsurancetable: UITableView!
    @IBOutlet var topsectionimage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkbtn = true
self.navigationController?.navigationBarHidden = true
        self.selectinsurancetable.delegate = self
        // Do any additional setup after loading the view.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    override func viewWillAppear(animated: Bool)
    {
        
        myMenuView.hidden = true
        self.blurview.hidden = true
        self.newview.hidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
   
    @IBAction func searchbtn(sender: AnyObject)
    {
        
        
    }
    
    
    
    @IBAction func addmorebtn(sender: AnyObject)
    {
        self.blurview.hidden = true
        self.newview.hidden = true
        
        
    }
    @IBAction func yesonlyonebtn(sender: AnyObject)
    
    {
        myMenuView.hidden = false

        let NewInsurance = self.storyboard!.instantiateViewControllerWithIdentifier("NewInsuranceViewController") as! NewInsuranceViewController
        
        self.navigationController?.pushViewController(NewInsurance, animated: true)
    }
    
    @IBAction func continuebtn(sender: AnyObject)
    {
        self.blurview.hidden = false
        self.newview.hidden = false
    }
    
    func tableView(tableView: UITableView, numberOfSectionsInTable sections:Int) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! SelectinsuranceTableCell
        
        cell.blankbtn.hidden = false
        cell.donebtn.hidden = true
        but.frame = CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, cell.contentView.frame.size.width, cell.contentView.frame.size.height)
        but.addTarget(self, action: "actionButton:", forControlEvents: .TouchUpInside)
        cell.addSubview(but)
        
        return cell
        
    }
    
    func actionButton(sender: UIButton)
    {
        indexpath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        let cell = selectinsurancetable.cellForRowAtIndexPath(indexpath) as! SelectinsuranceTableCell!
        
        
        if sender.tag == 0
        {
            if checkbtn == true
            {
                cell.blankbtn.hidden = true
                cell.donebtn.hidden = false
                
                
                checkbtn = false
            }
                
            else
            {
                
                cell.blankbtn.hidden = false
                cell.donebtn.hidden = true
                
                checkbtn = true
            }
            
            
        }
        
    }
 
    
    
    
    
    


}
