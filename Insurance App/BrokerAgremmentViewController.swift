//
//  BrokerAgremmentViewController.swift
//  Insurance App
//
//  Created by brst on 18/02/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class BrokerAgremmentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prefersStatusBarHidden() -> Bool {
        UIApplication.sharedApplication().statusBarHidden = true
        
        return true
    }

    
    @IBAction func backbtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func `continue`(sender: AnyObject)
    {
        let NewInsurance = self.storyboard!.instantiateViewControllerWithIdentifier("SignAgremmentViewController") as! SignAgremmentViewController
        
        self.navigationController?.pushViewController(NewInsurance, animated: true)

    }
   
}
